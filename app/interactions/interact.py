import os
import sys

import openai
from app.types import ChatCompletionMessage


def interact_with_chatgpt(model: str, messages: list[ChatCompletionMessage]) -> str:
    """Submit a list of pull requests as a prompt to ChatGPT."""
    api_key = os.getenv("OPENAI_API_KEY")

    if not api_key:
        sys.exit(
            "Unable to interact with ChatGPT: no OPENAI_API_KEY environment "
            "variable found."
        )

    openai.api_key = api_key

    completion = openai.ChatCompletion.create(
        model=model,
        messages=messages,
    )

    return completion["choices"][0]["message"]["content"]
