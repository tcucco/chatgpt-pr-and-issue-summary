from app.types import ChatCompletionMessage, PullRequest
from app.utils import pull_requests_to_messages

from .interact import interact_with_chatgpt

PROMPT: list[ChatCompletionMessage] = []


def stack_rank(
    model: str, pull_requests: list[PullRequest], max_body_length: int
) -> str:
    devs = {pr.author for pr in pull_requests}

    return interact_with_chatgpt(
        model,
        [
            {
                "role": "system",
                "content": (
                    "You are a helpful assistant that will help me rank all the "
                    "developers who work for my company."
                ),
            },
            {
                "role": "user",
                "content": (
                    "Based on the following contributions, return a list of the five "
                    "most valuable developers, and five least valuable developers, "
                    "based on the impact and importance of their contributions. And a "
                    "short description of why they were ranked as they were."
                ),
            },
            {
                "role": "user",
                "content": f"The full list of developers is {' '.join(devs)}",
            },
            *pull_requests_to_messages(
                pull_requests=pull_requests, max_body_length=max_body_length
            ),
        ],
    )
