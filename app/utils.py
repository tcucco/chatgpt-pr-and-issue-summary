import os
import sys
from typing import Iterator

from app.types import ChatCompletionMessage, Issue, PullRequest


def build_gh_request_headers(headers: dict[str, str] | None = None) -> dict[str, str]:
    """Attach an Authorization header to a headers dict."""
    token = os.getenv("GITHUB_API_TOKEN")

    if not token:
        sys.exit(
            "Unable to query github api: no GITHUB_API_TOKEN environment variable "
            "found."
        )

    base_headers = {"Authorization": f"Bearer {token}"}

    if headers:
        return headers | base_headers
    return base_headers


def pull_requests_to_messages(
    *,
    pull_requests: list[PullRequest],
    max_body_length: int,
    include_issues: bool = False,
) -> Iterator[ChatCompletionMessage]:
    """Converts a list of PullRequest instances to chat completion messages."""
    for pull_request in pull_requests:
        yield from _pull_request_to_messages(
            pull_request=pull_request,
            max_body_length=max_body_length,
            include_issues=include_issues,
        )


def _pull_request_to_messages(
    *, pull_request: PullRequest, max_body_length: int, include_issues: bool
) -> Iterator[ChatCompletionMessage]:
    """Converts a PullRequest and its closing issues to chat completion messages."""
    yield {
        "role": "user",
        "content": " ".join(
            [
                f"{pull_request.author} contributed" f'{pull_request.title}" ',
                (
                    f" - {pull_request.body[:max_body_length]}"
                    if max_body_length > 0
                    else ""
                ),
            ]
        ),
    }

    if include_issues:
        for issue in pull_request.closing_issues:
            yield _issue_to_message(issue, pull_request.author, max_body_length)


def _issue_to_message(
    issue: Issue, closer: str, max_body_length: int
) -> ChatCompletionMessage:
    """Converts an Issue instance to a chat completion message."""
    return {
        "role": "user",
        "content": (
            f'Issue {issue.number} by {closer} "{issue.title}" - '
            "{issue.body[:max_body_length]}"
        ),
    }
