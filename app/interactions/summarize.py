from app.types import ChatCompletionMessage, PullRequest
from app.utils import pull_requests_to_messages

from .interact import interact_with_chatgpt

PROMPT: list[ChatCompletionMessage] = [
    {
        "role": "system",
        "content": (
            "You're a helpful assistant who summarizes the work "
            "software engineering teams have performed the "
            "previous week."
        ),
    },
    {
        "role": "user",
        "content": (
            "I am a software developer at Close. A company that makes a CRM SaaS "
            "web app for sales organizations. Every week I need to summarize the "
            "work my team did the previous week and write up a short summary."
        ),
    },
    {
        "role": "user",
        "content": (
            "Based on the following contributions, summarize the "
            "changes we made to our product. Group similar items together, "
            "and explain why each change is important. Split the summary into "
            "two sections: changes that impact our customers, and changes that "
            "impact only our internal team."
        ),
    },
]


def summarize(
    model: str, pull_requests: list[PullRequest], max_body_length: int
) -> str:
    """Submit a list of pull requests as a prompt to ChatGPT."""
    return interact_with_chatgpt(
        model,
        [
            *PROMPT,
            *pull_requests_to_messages(
                pull_requests=pull_requests, max_body_length=max_body_length
            ),
        ],
    )
