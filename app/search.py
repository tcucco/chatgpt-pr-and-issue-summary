import logging
import sys
from datetime import datetime
from typing import Iterator

import httpx

from .types import Issue, PullRequest, SearchParams
from .utils import build_gh_request_headers

logger = logging.getLogger()


def search_pull_requests(params: SearchParams) -> Iterator[PullRequest]:
    (results, last_cursor) = get_pull_requests_page(params, None)

    yield from results

    while last_cursor:
        (results, last_cursor) = get_pull_requests_page(params, last_cursor)

        yield from results


def get_pull_requests_page(
    params: SearchParams, after_cursor: str | None
) -> tuple[list[PullRequest], str | None]:
    """Search for merged pull requests using GitHub GraphQL."""
    query = _build_pull_requests_search_query(params, after_cursor)

    response = httpx.post(
        "https://api.github.com/graphql",
        json={"query": query},
        headers=build_gh_request_headers(),
    )

    if not response.status_code == 200:
        sys.exit(f"Pull requests search returned {response.status_code}")

    edges = response.json()["data"]["search"]["edges"]
    nodes = [edge["node"] for edge in edges]

    last_cursor = edges[-1]["cursor"]

    return (
        [
            _parse_pull_request(node)
            for node in nodes
            if node["__typename"] == "PullRequest"
        ],
        last_cursor if len(nodes) == params.limit else None,
    )


def _parse_pull_request(node: dict) -> PullRequest:
    """Maps a pull request GraphQL node to a PullRequest instance."""
    issue_items = node["closingIssuesReferences"]["edges"]
    issue_nodes = (item["node"] for item in issue_items)

    return PullRequest(
        repository=node["repository"]["name"],
        id=node["id"],
        number=node["number"],
        author=node["author"]["login"],
        state=node["state"],
        title=node["title"],
        body=node["body"],
        merged_at=datetime.fromisoformat(node["mergedAt"])
        if node["mergedAt"]
        else None,
        closing_issues=[_parse_issue(issue_node) for issue_node in issue_nodes],
    )


def _parse_issue(node: dict) -> Issue:
    """Maps n issue GraphQL node to an Issue instance."""
    return Issue(
        repository=node["repository"]["name"],
        id=node["id"],
        number=node["number"],
        title=node["title"],
        body=node["body"],
    )


def _build_pull_requests_search_query(params: SearchParams, after_cursor: str | None):
    """Builds a GitHub GraphQL search for merged PRs.

    See https://docs.github.com/en/search-github/searching-on-github/searching-issues-and-pull-requests
    for more information.
    """
    query = " ".join(
        [
            "is:pr",
            "is:merged",
            f"merged:{params.from_date}..{params.to_date}",
            *(f"repo:{repo}" for repo in params.repos),
            *(f"author:{user}" for user in params.users),
        ]
    )

    search_args = [
        "type:ISSUE",
        f'query:"{query}"',
        f"first:{params.limit}",
    ]
    if after_cursor:
        search_args.append(f'after:"{after_cursor}"')
    search_args_str = " ".join(search_args)

    logger.info(f'Searching issues with query "{query}"')
    logger.info(f'Search args "{search_args_str}"')

    return f"""
query {{
    search({search_args_str}) {{
        edges {{
            cursor
            node {{
                __typename
                ... on PullRequest {{
                    repository {{
                        name
                    }}
                    author {{
                        login
                    }}
                    id
                    number
                    title
                    body
                    state
                    mergedAt
                    closingIssuesReferences(first:5) {{
                        edges {{
                            node {{
                                repository {{
                                    name
                                }}
                                id
                                number
                                title
                                body
                            }}
                        }}
                    }}
                }}
            }}
        }}
    }}
}}"""
