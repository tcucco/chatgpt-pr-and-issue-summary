# ChatGPT PR and Issue Summary

This code uses the GitHub GraphQL API to pull PRs merged for a given time
period along with their closing issues, and passes those to ChatGPT to generate
a summary.

## Installation

1. Clone this repo
2. `pip install -r requirements.txt`

## Invocation

You will need two environment variables defined:

- `GITHUB_API_TOKEN`
- `OPENAI_API_KEY`

The GitHub token must have access to read Pull Requests on the GraphQL API
endpoint.

```
python3 main.py -r repo_1 -r repo_2 -u user_1 -u user_2
```

Try `python3 main.py -h` to see all options
