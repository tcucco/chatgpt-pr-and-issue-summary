from dataclasses import dataclass
from datetime import date, datetime
from typing import TypedDict

ChatCompletionMessage = TypedDict(
    "ChatCompletionMessage",
    {
        "role": str,
        "content": str,
    },
)


@dataclass(kw_only=True, frozen=True)
class SearchParams:
    repos: list[str]
    users: list[str]
    from_date: date
    to_date: date
    limit: int = 100


@dataclass(kw_only=True, frozen=True)
class Issue:
    repository: str
    id: str
    number: int
    title: str
    body: str


@dataclass(kw_only=True, frozen=True)
class PullRequest:
    repository: str
    id: str
    number: int
    author: str
    state: str
    title: str
    body: str
    merged_at: datetime | None
    closing_issues: list[Issue]
