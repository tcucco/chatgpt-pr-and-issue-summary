import argparse
import logging
import sys
from datetime import date, timedelta

from app.interactions import interact
from app.search import search_pull_requests
from app.types import SearchParams

MODEL = "gpt-3.5-turbo"


def parse_args():
    """Parse command line arguments."""
    default_to_date = date.today()
    default_from_date = default_to_date - timedelta(7)

    parser = argparse.ArgumentParser(
        description="Summarize a project from closed issues and merged PRs"
    )
    parser.add_argument(
        "action",
        choices=("summarize", "stack_rank"),
    )
    parser.add_argument(
        "-r",
        "--repo-name",
        action="append",
        required=True,
        help="Name of a repository to include in analysis",
    )
    parser.add_argument(
        "-u",
        "--user-name",
        action="append",
        help="Name of a user to include in analysis",
    )
    parser.add_argument(
        "-f",
        "--from-date",
        type=date.fromisoformat,
        default=default_from_date,
        help="Minimum date for the analysis range",
    )
    parser.add_argument(
        "-t",
        "--to-date",
        type=date.fromisoformat,
        default=default_to_date,
        help="Maximum date for the analysis range",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        default=0,
        help="Verbosity level of the program",
    )
    parser.add_argument(
        "-l",
        "--limit",
        type=int,
        default=100,
        help="Number of GH PRs to pull per request",
    )
    parser.add_argument(
        "-m",
        "--max-body-length",
        type=int,
        default=10000,
        help="Max body length for issues and PRs",
    )
    return parser.parse_args()


def main():
    """Main entry point for the program."""
    args = parse_args()

    configure_logging(args.verbose)

    params = SearchParams(
        repos=args.repo_name,
        users=args.user_name or [],
        from_date=args.from_date,
        to_date=args.to_date,
        limit=args.limit,
    )

    pull_requests = list(search_pull_requests(params))
    response = interact(
        model=MODEL,
        action=args.action,
        pull_requests=pull_requests,
        max_body_length=args.max_body_length,
    )

    print(response)


def configure_logging(verbosity: int):
    """Configure logging baed on the user-requested verbosity level."""
    level: int = 0

    match verbosity:
        case 0:
            level = logging.ERROR
        case 1:
            level = logging.WARNING
        case 2:
            level = logging.INFO
        case _:
            level = logging.DEBUG

    logging.basicConfig(level=level, stream=sys.stderr)


if __name__ == "__main__":
    main()
