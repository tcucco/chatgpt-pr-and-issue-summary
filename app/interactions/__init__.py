from typing import Literal

from app.types import PullRequest

from .stack_rank import stack_rank
from .summarize import summarize

Action = Literal["summarize"] | Literal["stack_rank"]


def interact(
    *,
    model: str,
    action: Action,
    pull_requests: list[PullRequest],
    max_body_length: int = 10000,
) -> str:
    match action:
        case "summarize":
            return summarize(model, pull_requests, max_body_length)
        case "stack_rank":
            return stack_rank(model, pull_requests, max_body_length)
        case _:
            raise NotImplementedError(f"interaction {action} not implemented")
